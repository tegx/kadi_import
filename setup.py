import os
import shutil

from setuptools import find_packages
from setuptools import setup

__version__ = "0.1"


console_scripts = ["kadi-import-json-schema = kadi_import:kadi_import_json_schema"]


setup(
    name="kadi_import",
    version=__version__,  # noqa: F821, pylint:disable=undefined-variable
    license="Apache 2.0",
    author="Arnd Koeppe",
    description="An import script for JSON schema to Kadi.",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.8",
    zip_safe=False,
    install_requires=[
        "flask",
        "flask_login",
        "jsonref",
        "kadi_apy",
        "xmlhelpy",
    ],
    entry_points={"console_scripts": console_scripts},
)
