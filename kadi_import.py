# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import re
from copy import deepcopy
from pathlib import Path

import jsonref
from flask import json
from flask_login import current_user
from kadi_apy import KadiManager
from xmlhelpy import command
from xmlhelpy import option


def read_json_data(file):
    """Import a template from a JSON file.

    See :func:`read_json_data` for an explanation of the parameters and return value.
    """
    # Read json file
    file = Path(file)
    with file.open("r", encoding="utf-8") as f:
        return json.load(f)


STATIC_TYPE_MAPPING = {"integer": "int", "number": "float", "boolean": "bool"}


def _json_schema_to_extras(extras_schema, required=None):

    required = required or []

    def map_fun(key, value, required=True):
        # Key
        extra = {}
        if key is not None:
            extra["key"] = key

        # Default value
        default = extra.get("default")
        if default is not None:
            extra["default"] = default

        # Original type and required
        value_type = value["type"]

        # Handle required
        if isinstance(value_type, (list, tuple)):
            if value_type[-1] is None:
                required = False
            value_type = value_type[0]

        # Map type
        if value_type == "object":
            result = _json_schema_to_extras(value["properties"], required=value.get("required"))
            extra["type"] = "dict"
            extra["value"] = result
        elif value_type == "array":
            result = _json_schema_to_extras([value["items"]])
            extra["type"] = "list"
            extra["value"] = result
        else:
            if value_type == "string":
                # String or date type
                if value.get("format") == "date-time":
                    extra["type"] = "date"
                else:
                    extra["type"] = "str"
            else:
                extra["type"] = deepcopy(STATIC_TYPE_MAPPING[value_type])

            # Validation
            validation = {}
            if required:
                validation["required"] = required

            choices = value.get("enum")
            if choices is not None:
                validation["options"] = list(choices)

            maximum = value.get("maximum")
            minimum = value.get("minimum")
            if maximum is not None or minimum is not None:
                validation["range"] = {"max": maximum, "min": minimum}

            if validation:
                extra["validation"] = validation

        return extra

    if isinstance(extras_schema, (list, tuple)):
        return [map_fun(None, value, required=False) for value in extras_schema]
    return [
        map_fun(key, value, required=key in required)
        for key, value in extras_schema.items()
    ]


def parse_json_schema_data(path, identifier=None, template_type="record"):
    """Import a template from a JSON Schema file.

    See :func:`read_json_schema_data` for an explanation of the parameters and return value.
    """
    # Read json file
    path = Path(path)
    with path.open("r", encoding="utf-8") as f:
        json_schema = jsonref.load(f)
    # Use schema and id to link / decide on processing?
    identifier = identifier or json_schema["$id"]
    identifier = re.sub("[^A-Za-z0-9-_]+", "--", identifier).lower()

    # Parse fixed record metadata from json schema root
    template_data = {
        "title": json_schema["title"],
        "identifier": identifier,
        "description": json_schema["description"],
    }

    # Parse properties
    extras = [
        {"key": "$schema", "type": "str", "value": json_schema["$schema"]},
    ]
    _id = json_schema.get("$id")
    if _id:
        extras.append({"key": "$id", "type": "str", "value": _id})

    extras.extend(
        _json_schema_to_extras(
            json_schema["properties"], required=json_schema.get("required")
        )
    )

    if template_type == "record":
        # TODO: need to add default values?
        # for key, value in record_properties.items():
        #     # Skip all falsy default values.
        #     if template_data["data"].get(key):
        #         value["default"] = template_data["data"][key]

        # Add the extra metadata to the record properties.
        template_data["type"] = "record"
        template_data["data"] = {"license": None, "tags": [], "extras": extras}

    elif template_type == "extras":
        template_data["type"] = "extras"
        template_data["data"] = extras

    return template_data


def read_import_data(file, file_type, identifier=None, template_type="record"):
def read_import_data(path, file_format=None, identifier=None, template_type="record"):
    """Import a template from a file with given format."""
    if file_format is None:
        file_format = str(path).rsplit(".", 1)[-1]

    if file_format == "json":
        return parse_json_schema_data(
            path, identifier=identifier, template_type=template_type
        )

    return None


@command(
    "kadi-import-template",
    version="0.1",
    description="Import an metadata schema into a Kadi Template.",
    example="""
        >>> kadi-import-metadata-schema --file example.xsd
        >>> kadi-import-metadata-schema --file example.json --identifier my-identifier
        """,
)
@option(
    "path",
    description="A file (xsd, json schema) with a metadata template.",
    required=True,
    char="p",
)
@option(
    "file_format",
    description="A file format, such as xsd or json for metadata templates.",
    default=None,
    required=False,
    char="f",
)
@option(
    "identifier",
    description="A Kadi identifier for the template.",
    default=None,
    required=False,
    char="i",
)
@option(
    "template_type",
    description="Type of template to create ('record' or 'extra')",
    default="record",
    required=False,
    char="t",
)
def kadi_import_template(
    path, file_format=None, identifier=None, template_type="record"
):
    data = read_import_data(
        path,
        file_format=file_format,
        identifier=identifier,
        template_type=template_type,
    )
    manager = KadiManager()
    manager.template(create=True, **data)


if __name__ == "__main__":
    kadi_import_template.callback(
        "example2.json", identifier="debug-record-template", template_type="record"
    )
    kadi_import_template.callback(
        "example.json", identifier="debug-extras-template", template_type="extras"
    )
    kadi_import_template.callback(
        "datacite-metadata.json",
        # "http://schema.datacite.org/meta/kernel-4/metadata.xsd",
        identifier="debug-datacite-template",
        template_type="record",
    )
